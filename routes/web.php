<?php

Route::get('/', function () {
    return view('index');
});

Route::get('/create', function () {
    return view('product/create');
});

Route::get('/table', 'userController@table');
